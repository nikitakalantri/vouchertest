Feature: Test CRUD method to create and fetch Vouchers

  Scenario: Add Voucher record
    Given I set post voucher service api endpoint
    When Send a POST HTTP request
    Then I receive valid response

  Scenario: Client makes call to GET /api/v1/fetchVoucher
    Given the client calls get api
    When the client calls /api/v1/fetchVoucher
    Then the client receives status code of 200 OK

  Scenario: Fetch voucher by Id
    Given I set get voucher service api endpoint
    When Send a GET HTTP request
    Then the client receives valid response
