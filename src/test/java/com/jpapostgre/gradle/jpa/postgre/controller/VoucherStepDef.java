package com.jpapostgre.gradle.jpa.postgre.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.jpapostgre.gradle.jpa.postgre.model.VoucherModel;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VoucherStepDef {

	VoucherController voucherController;
	RestTemplate restTemplate = new RestTemplate();
	HttpHeaders headers = new HttpHeaders();
	String baseUrl;
	ResponseEntity<String> responseEntity;
	ResponseEntity<String> responseEn;
	ResponseEntity<String> response;

	@Given("I set post voucher service api endpoint")
	public void i_set_post_voucher_service_api_endpoint() {
		baseUrl = "http://localhost:8090" + "/api/v1/createVoucher";
	}

	@When("Send a POST HTTP request")
	public void send_a_POST_HTTP_request() {
		VoucherModel voucherModel = new VoucherModel(1, "Personal", "50", "5000");
		responseEntity = this.restTemplate.postForEntity(baseUrl, voucherModel, String.class);
	}

	@Then("I receive valid response")
	public void i_receive_valid_response() {
		assertEquals(200, responseEntity.getStatusCodeValue());
	}

	@Given("the client calls get api")
	public void the_client_calls_get_api() {
		voucherController = new VoucherController();
	}

	@When("the client calls \\/api\\/v{int}\\/fetchVoucher")
	public void the_client_calls_api_v_fetchVoucher(Integer int1) {
		response = restTemplate.getForEntity("http://localhost:8090" + "/api/v1/fetchVoucher", String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Then("the client receives status code of {int} OK")
	public void the_client_receives_status_code_of_OK(Integer int1) {
		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Given("I set get voucher service api endpoint")
	public void i_set_get_voucher_service_api_endpoint() {
		baseUrl = "http://localhost:8090" + "api/v1/fetchVoucher/6";
	}

	@When("Send a GET HTTP request")
	public void send_a_GET_HTTP_request() {
		responseEn = restTemplate.getForEntity(baseUrl, String.class);
		assertEquals(HttpStatus.OK, responseEn.getStatusCode());
	}

	@Then("the client receives valid response")
	public void the_client_receives_valid_response() {
		Assertions.assertEquals(HttpStatus.OK, responseEn.getStatusCode());
	}

}
